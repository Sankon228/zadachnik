#include <iostream>
#include <cmath>
using namespace std;

int beg_1();
int beg_2();
int beg_3();
int beg_4();
int beg_24();
int int_7();
int nom_1();
int bool_6();
int bool_16();
int bool_17();
int bool_18();
int bool_30();
int bool_33();
int bool_34();
void if_1(int, int, int);
void if_5(int, int, int);
void if_6(int, int);
void if_11(int, int);
void if_12();
void if_28();
int nom_bool2();
int case_1();
int case_8();
int case_18();
int for_11();
int for_18();
int fact(int);
int for_23();
int for_33();
int for_36();
int while_1();
int while_3();
int while_16();
int while_17();
int while_24();
int while_30();
int main() {
    cout << while_30();
}
int nom_1() {
    int a;
    cin >> a;
    if (99 < a < 1000) {
        int ans;
        ans = a % 10 * 100 + a / 10;
        return ans;
    }
    else {
        return 0;
    }
}
int nom_bool2() {

    int a, b, c;
    cout << "a"; cin >> a;
    cout << "b"; cin >> b;
    cout << "c"; cin >> c;
    bool ans = a < b&& b < c;
    return ans;
}
int beg_1() {
    int p, a;
    cin >> a;
    p = 4 * a;
    return p;
}
int beg_2() {
    int S, a, b, P;
    cin >> a;
    cin >> b;
    S = a * b;
    P = 2 * (a + b);
    cout << P << '\n';
    cout << S;
    return 0;
}
int beg_3() {
    int a, v, s;
    cin >> a;
    v = a * a * a;
    s = 6 * a * 2;
    cout << v << '\n';
    cout << s << '\n';
    return 0;
}
int beg_4() {
    int ans, a, b;
    cin >> a;
    cin >> b;
    ans = sqrt(a * b);
    return ans;
}
int beg_24() {
    int a, b, c, d;
    cin >> a;
    cin >> b;
    cin >> c;
    d = a;
    a = b;
    b = c;
    c = d;
    cout << a << '\n';
    cout << b << '\n';
    cout << c << '\n';
    return 0;
}
int int_7() {
    int a, ans, c, b;
    string e;
    e = "error";
    cin >> a;
    if (9 < a < 100) {
        c = a / 10;
        b = a % 10;
        ans = c + b;
        return ans;
    }
    else {
        return 0;
    }
}
int bool_6() {
    int a, b, c;
    cin >> a;
    cin >> b;
    cin >> c;
    bool d;
    d = a < b&& b < c;
    return d;
}
int bool_16() {
    bool ans;
    int a;
    cin >> a;
    ans = a % 2 == 0 && 9 < a && a < 100;
    return ans;
}
int bool_17() {
    bool ans;
    int a, b, c, d;
    cin >> a;
    b = a / 100;
    c = (a % 100) / 10;
    d = a % 10;
    ans = b < c&& c < d;
    return ans;
}
int bool_18() {
    bool ans;
    int a, b, c, d;
    cin >> a;
    b = a / 100;
    c = (a % 100) / 10;
    d = a % 10;

    ans = (b == c) || (c == d) || (b == d) && (99 < a) && (a < 1000);
    return ans;
}
int bool_30() {
    int a, b, c;
    bool ans;
    cin >> a;
    cin >> b;
    cin >> c;
    ans = a == b && b == c && a == c;
    return ans;
}
int bool_33() {
    int a, b, c;
    bool ans;
    cin >> a;
    cin >> b;
    cin >> c;
    ans = a <= b + c && b <= c + a && c <= b + a;
    return ans;
}
int bool_34() {
    bool ans;
    int a, b;
    cin >> a;
    cin >> b;
    ans = ((a % 2 == 0 && b % 2 == 1) || (a % 2 == 1 && b % 2 == 0)) && 0 < a && a < 9;
    return ans;
}
void if_1() {
    int a, ans;
    cin >> a;
    if (0 < a) {
        a++;
        ans = a;
        cout << ans;
    }
    else {
        cout << "error";
    }
}
void if_5(int a, int b, int c) {
    int ans = 0;
    if (0 < a) { ans++; }
    if (0 < b) { ans++; }
    if (0 < c) { ans++; }
    cout << ans;
}
void if_6(int a, int b) {
    if (a < b) {
        cout << b;
    }
    else if (a > b) { cout << a; }
    else {
        cout << "equal";
    }
}
void if_11(int a, int b) {
    if (a == b) {
        a = 0; b = 0; cout << a << "\t" << b;
    }
    else if (a > b) { b = a; cout << a << "\t" << b; }
    else { a = b; cout << a << "\t" << b; }
}
void if_12() {
    double a, ans;
    cin >> a;
    if (a > 0) {
        ans = sin(a); cout << ans << "\t";
    }
    else { ans = 6 - a; cout << ans << "\t"; }
}
void if_28() {
    int a, ans;
    cin >> a;
    if (a % 400 == 0 || a % 4 == 0 && a % 100 != 0) {
        ans = 366; cout << ans << "\t";
    }
    else { ans = 365; cout << ans << "\t"; }
}
int case_1() {
    int x;
    cin >> x;
    switch (x)
    {
    case 1:
        cout << "Monday" << "\n";
        break;
    case 2:
        cout << "Tuesday" << "\n";
        break;
    case 3:
        cout << "Wednesday" << "\n";
        break;
    case 4:
        cout << "Thursday" << "\n";
        break;
    case 5:
        cout << "Friday" << "\n";
        break;

    case 6:
        cout << "Saturday" << "\n";
        break;
    case 7:
        cout << "Sunday" << "\n";
        break;
    }
    return 0;
}
int case_8() {
    int d, month;
    cin >> d;
    cin >> month;
    d = d - 1;
    if (d == 0) {
        month = month - 1;
        if (month == 0) {
            month = 12;
        }
        switch (month)
        {
        case 1: d = 31;
        case 2: d = 30;
        case 3: d = 31;
        case 4: d = 30;
        case 5: d = 31;
        case 6: d = 30;
        case 7: d = 31;
        case 8: d = 31;
        case 9: d = 30;
        case 10: d = 31;
        case 11: d = 30;
        case 12: d = 31;
        }
    }
    cout << month << "\t" << d;
    return 0;
}
int case_18() {
    int day, a, b, c;
    string a1, b1,b2, c1;
    cin >> day;
    if (99 < day < 1000) {
        a = day / 100;
        switch (a)
        {
        case 1: a1 = "������ ";        break;
        case 2: a1 = "�t�r�u�����u ";     break;
        case 3: a1 = "�����y�����p ";     break;
        case 4: a1 = " ���u�������y�����p";  break;
        case 5: a1 = "������������";      break;
        case 6: a1 = "���u������������";    break;
        case 7: a1 = "���u�}������";      break;
        case 8: a1 = "�r�����u�}������";    break;
        case 9: a1 = "�t�u�r������������";   break;
        }
        b = (day % 100) / 10;
        c = day % 10;
        switch (c)
        {
        case 1: b2 = "���t�y�~�p�t���p����";   break;
        case 2: b2 = "�t�r�u�~�p�t���p����";   break;
        case 3: b2 = "�����y�~�p�t���p����";   break;
        case 4: b2 = "���u�������~�p�t���p����"; break;
        case 5: b2 = "�������~�p�t���p������";  break;
        case 6: b2 = "���u�����~�p�t���p����";  break;
        case 7: b2 = "���u�}�~�p�t���p����";   break;
        case 8: b2 = "�r�����u�}�~�p�t���p����"; break;
        case 9: b2 = "�t�u�r�����~�p�t���p����"; break;
        }
        switch (b)
        {
        case 2: b1 = "�t�r�p�t���p����";    break;
        case 3: b1 = "�����y�t���p����";    break;
        case 4: b1 = "���������{";       break;
        case 5: b1 = "�������t�u����";     break;
        case 6: b1 = "���u�����t�u������";   break;
        case 7: b1 = "���u�}�t�u������";    break;
        case 8: b1 = "�r�����u�}�t�u������";  break;
        case 9: b1 = "�t�u�r���~��������";   break;
        case 0: b1 = "";            break;
        }
        switch (c)
        {
        case 1: c1 = "���t�y�~";   break;
        case 2: c1 = "�t�r�p";    break;
        case 3: c1 = "�����y";    break;
        case 4: c1 = "���u�������y"; break;
        case 5: c1 = "��������";   break;
        case 6: c1 = "���u������";  break;
        case 7: c1 = "���u�}��";   break;
        case 8: c1 = "�r�����u�}��"; break;
        case 9: c1 = "�t�u�r������"; break;
        case 0: c1 = ""; break;

            cout << a << "\t" << b << "\t" << c;
        }
        if (b == 1) {
            cout << a1 << "\t" << b2 << "\t" << c1;
        }
        else {
            cout << a1 << "\t" << b1<< "\t" << c1;

        }
    }
    return 0;
}
int for_11() {
    int n;
    cin >> n;

    float ans = 0;
    for (int i = 0; i <= n; i++)
        ans += pow(n + i, 2);

    cout << ans;
    return 0;
}
int for_18() {
    int ans=1, n , a,a1;
    cin >> n;
    cin >> a;
    a1 = 1;
    for (int i = 1; i <= n; i++) {
        a1 *= -1;
        ans += pow(a, i)*a1;
    }
    return ans;
}
int fact(int N)
{
    if (N < 0) {
        return 0;
    }
    if (N == 0) {
        return 1;
    }
    else {
        return N * fact(N - 1);
    }
}
int for_23() {
    int x,x1, n, ans;
    cin >> n;
    cin >> x;
    x1 = 1;
    ans = x;
    for (int i = 1; i <= n; i=i+2) {
        x1 *= -1;
        ans += pow(x, i)*x1 / fact(i);
    }
    return ans;
}
int for_33() {
    int a, n,a1, a2, ans;
    a1 = 1;
    a2 = 1;
    cin >> n;
    for (int i = 3; i <= n; i++) {
        a = a1 + a2;
        a1 = a2;
        a2 = a;
    }
    return a;
}
int for_36() {
    int n, k, ans;
    cin >> n;
    cin >> k;
    ans = 0;
    for (int i = 1; i <= n; i++) {
        ans += pow(i, k);
    }
    return ans;
}
int while_1() {
    int a, b, ans;
    cin >> a;
    cin >> b;
    ans = 0;
    while (a >b) {
        a -= b;

    }
    return a;
}
int while_3() {
    int n,k, ans;
    cin >> n;
    cin >> k;
    int num = 0;
    while (n >= k) {
        n -= k;
        ++num;
    }
    cout << num << "\t";
    cout << n << "\t";
    return 0;
}int while_16() {
    float p;
    cin >> p;
    int k = 1;
    float d = 10, s = 10;
    while (s <= 200) {
        ++k;
        d += d * p / 100;
        s += d;
    }
    cout << k << "\n" << s;
    return 0;
}
int while_17() {
    int n;
    cin >> n;
    while (n > 0) {
        cout << n % 10;
        n /= 10;
    }
    return 0;
}
int while_24() {
    int n;
    cin >> n;

    int a1 = 1, a2 = 1,a = 0;

    while (a < n) {
        a = a2 + a1;
        a2 = a1;
        a1 = a;
    }
    if (a == n)
        cout << "True";
    else
        cout << "False";
    return 0;
}
int while_30() {
    int a, b, c;
    cin >> a;
    cin >> b;
    cin >> c;
    int ans = 0;
    while (b - c >= 0) {
        b -= c;
        while (a - c >= 0)  {
            a -= c;
            ans++;
        }
    }
    cout << ans;
    return 0;
}